//
// Created by TomOwen on 04/02/2021.
//

#ifndef TESTFOO_BLINKY_H
#define TESTFOO_BLINKY_H

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"

extern osThreadId_t blink01Handle;
extern osThreadId_t blink02Handle;

void StartBlink01(void *argument);
void StartBlink02(void *argument);

#endif //TESTFOO_BLINKY_H

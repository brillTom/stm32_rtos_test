//
// Created by TomOwen on 04/02/2021.
//

#include "../Inc/blinky.h"

osThreadId_t blink01Handle;
osThreadId_t blink02Handle;

/* USER CODE BEGIN Header_StartBlink01 */
/**
  * @brief  Function implementing the blink01 thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartBlink01 */
void StartBlink01(void *argument)
{
    /* USER CODE BEGIN 5 */
    /* Infinite loop */
    for(;;)
    {
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13);
        osDelay(500);
    }

    // In case we accidentally exit from task loop
    osThreadTerminate(NULL);

    /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartBlink02 */
/**
* @brief Function implementing the blink02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartBlink02 */
void StartBlink02(void *argument)
{
    /* USER CODE BEGIN StartBlink02 */
    /* Infinite loop */
    for(;;)
    {
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13);
        osDelay(600);
    }

    // In case we accidentally exit from task loop
    osThreadTerminate(NULL);

    /* USER CODE END StartBlink02 */
}
#!/bin/bash
set -e

DEFAULT_FILE="test-blinky-baremetal/build/test-blinky-baremetal.hex"
DEFAULT_HOST='127.0.0.1'

FILE=${1-$DEFAULT_FILE}
HOST=${2-$DEFAULT_HOST}

if [ ! -f $FILE ]; then
  echo $FILE not found
  exit 1
fi

echo flashing to GDB target on $HOST

gdb-multiarch -nx --batch \
-ex "target remote tcp:${HOST}:3333" \
-x gdb_flash.gdbscr $FILE | grep matched

# wake the target
sleep 1
echo "reset run" | nc -q 0 $HOST 4444

